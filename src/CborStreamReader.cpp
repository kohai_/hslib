#include "CborStreamReader.hpp"
#include "HS_bit.hpp"
#include <assert.h>
#include <iostream>

namespace {
  bool skipInt(CborStreamReader& r)
  {
    return true;
  }

  unsigned char detectAdditional(char v)
  {
    //return v xor 0b000'111000;
    //return 1 << (v - 24);
    v = v bitand 0b000'11111;
    if (v < 24)
      return 0;
    else if (v == 24)
      return 1;
    else if (v == 25)
      return 2;
    else if (v == 26)
      return 4;
    else if (v == 28)
      return 8;
    else
      return 0;
  }



  bool skipSingleArray(CborStreamReader& r)
  {
    while (r.hasNext())
    {
      if (not r.next())
      {
        return false;
      }
    }

    return true;
  }


  template<typename T>
  struct ParsingResult
  {
    T value;
    CborStreamReaderError err;
  };

  ParsingResult<unsigned long> parseUnsigned(char const* data, unsigned int index, unsigned int size)
  {
    ParsingResult<unsigned long> res { 0ul, CborStreamReaderError::NoError };
    if (index >= size)
    {
      res.err = CborStreamReaderError::IndexOutOfRange;
      return res;
    }
    char const* readPtr = data + index;
    unsigned char additional = detectAdditional(*readPtr);

    if (index + additional >= size)
    {
      res.err = CborStreamReaderError::EndOfFile;
      return res;
    }

    unsigned long ret = ((unsigned char) (*readPtr) bitand 0b000'11111);
    char* p = (char*) &ret;
    readPtr += 1;

    for (int i = 0; i < additional; ++i)
    {
      if constexpr (HS::Endian::native == HS::Endian::big)
      {
        p[i] = readPtr[i];
      }
      else
      {
        p[i] = readPtr[additional - 1 - i];
      }
    }

    res.value = ret;
    return res;
  }
}

CborStreamReader::CborStreamReader(std::string const& data)
{
  buffer[0] = data;
  mainBufferPtr = std::string_view(buffer[0].begin(), buffer[0].end());
}

CborStreamReader::~CborStreamReader()
{
}

void CborStreamReader::addData(std::string const& other)
{
  int oldBufIdx = (int) mainBuffer;
  int newBufIdx = (int) !mainBuffer;

  std::string& newBuffer = buffer[oldBufIdx];
  std::string& oldBuffer = buffer[newBufIdx];

  newBuffer.clear();
  newBuffer.append(mainBufferPtr.begin(), mainBufferPtr.end());
  newBuffer.append(other);

  mainBufferPtr = std::string_view(newBuffer);

  if (tmpReader != nullptr)
    tmpReader->addData(other);
}


CborStreamReader::Type CborStreamReader::type()
{
  if (!mainBufferPtr.empty())
  {
    char byte = mainBufferPtr.front();
    unsigned char typeData = byte bitand 0b111'00000;
    switch (typeData)
    {
      case 0b000'00000:
        return Type::UnsignedInteger;
      case 0b001'00000:
        return Type::NegativeInteger;
      case 0b010'00000:
        return Type::ByteString;
      case 0b011'00000:
        return Type::TextString;
      case 0b100'00000:
        return Type::Array;
      case 0b101'00000:
        return Type::Map;
      case 0b110'00000:
        return Type::Tag;
      case 0b111'00000:
        return Type::Float;
      default:
        assert(0);
    }
  }
  return Type::Invalid;
}


unsigned int CborStreamReader::depth()
{
  return mContainerStack.size();
}

bool CborStreamReader::enterContainer()
{
  if (type() == Type::Array or type() == Type::Map)
  {
    unsigned char argumentWidth = detectAdditional(mainBufferPtr.front());
    if (mainBufferPtr.size() >= 1 + argumentWidth)
    {
      auto item = ContainerStackItem {
        type(),
        isIndefiniteLength(),
        length(),
        0ul
      };
      mContainerStack.push_back(item);
      mainBufferPtr.remove_prefix(1 + argumentWidth);
      return true;
    }
    else
    {
      mLastError = CborStreamReaderError::EndOfFile;
    }
  }

  return false;
}

bool CborStreamReader::leaveContainer()
{
  if (mContainerStack.empty())
  {
    return false;
  }
  else if (mContainerStack.back().isIndefiniteLength)
  {

    if (mainBufferPtr.front() == (char)0b111'11111)
    {
      mainBufferPtr.remove_prefix(1);
      return true;
    }
    else
    {
      mLastError = CborStreamReaderError::InvalidOperation;
      return false;
    }
  }
  else if (mContainerStack.back().nItems == mContainerStack.back().cItems)
  {
    return true;
  }
  else
  {
    mLastError = CborStreamReaderError::InvalidOperation;
    return false;
  }
}

unsigned int const static chunkSize = 64;
CborStreamReader::StringResult CborStreamReader::readByteString()
{
  StringResult r {};
  unsigned long l = length();
  unsigned char additional = detectAdditional(mainBufferPtr.front());
  unsigned int shorter = l > chunkSize ? chunkSize : l;

  if (shorter + 1 + additional >= mainBufferPtr.size())
  {
    r.err = StringResult::Status::Error;
    mLastError = CborStreamReaderError::EndOfFile;
    return r;
  }
  

  mainBufferPtr.remove_prefix(1 + additional);
  r.data = std::string (mainBufferPtr.substr(0, shorter));
  r.len = l - shorter;

  r.err= r.len > 0 ? StringResult::Status::Ok : StringResult::Status::EndOfString;
  mainBufferPtr.remove_prefix(shorter);

  return r;
}

CborStreamReader::StringResult CborStreamReader::readByteString(StringResult& r)
{
  unsigned int shorter = r.len > chunkSize ? chunkSize : r.len;

  if (shorter >= mainBufferPtr.size())
  {
    r.err = StringResult::Status::Error;
    mLastError = CborStreamReaderError::EndOfFile;
    return r;
  }
  

  r.data = std::string (mainBufferPtr.substr(0, shorter));
  r.len -= shorter;

  r.err= r.len > 0 ? StringResult::Status::Ok : StringResult::Status::EndOfString;
  mainBufferPtr.remove_prefix(shorter);

  return r;
}

CborStreamReader::StringResult CborStreamReader::readTextString()
{
  return readByteString();
}

CborStreamReader::StringResult CborStreamReader::readTextString(StringResult& r)
{
  return readByteString(r);
}


unsigned long CborStreamReader::toUnsigned()
{
  auto res = parseUnsigned(mainBufferPtr.data(), 0, mainBufferPtr.size());
  mLastError = res.err;
  return res.value;
}

long CborStreamReader::toNegative()
{
  return 0;
}


bool CborStreamReader::hasNext()
{
  if (mContainerStack.empty())
  {
    return false;
  }
  return mContainerStack.back().nItems != mContainerStack.back().cItems || mContainerStack.back().isIndefiniteLength;
}

bool CborStreamReader::next()
{
  if (mainBufferPtr.empty())
    return false;

  if (tmpReader != nullptr)
  {
    bool res = skipSingleArray(*tmpReader);
    if (not res)
    {
      return false;
    }
    else
    {
      if (not tmpReader->leaveContainer())
        return false;
      // TODO (hanasou): advance current container and reduce number of item
      // if this reader is nested
      delete tmpReader;
      tmpReader = nullptr;
    }
  }
  else if (mHasStoredState)
  {
    unsigned long& remain = mStoredState.length;
    if (remain < mainBufferPtr.size())
    {
      mainBufferPtr.remove_prefix(remain);
      mHasStoredState = false;
    }
    else
    {
      remain -= mainBufferPtr.size();
      mainBufferPtr.remove_prefix(mainBufferPtr.size());
    }
  }
  else
  {
    switch (type())
    {
      case UnsignedInteger:
      case NegativeInteger:
      case Float:
        {
          unsigned long additional = detectAdditional(mainBufferPtr.front());
          if (mainBufferPtr.size() < 1 + additional)
          {
            mLastError = CborStreamReaderError::EndOfFile;
            return false;
          }
          mainBufferPtr.remove_prefix(1 + additional);
        }
        break;
      case Array:
        {
          tmpReader = new CborStreamReader(std::string(mainBufferPtr.begin(), mainBufferPtr.end()));
          assert(tmpReader->isArray());
          tmpReader->enterContainer();
          bool res = skipSingleArray(*tmpReader);
          if (not res)
          {
            return false;
          }
          else
          {
            if (not tmpReader->leaveContainer())
              return false;
            // TODO (hanasou): advance current container and reduce number of item
            // if this reader is nested
            delete tmpReader;
            tmpReader = nullptr;
          }
        }
        break;
      case ByteString:
      case TextString:
        {
          unsigned long bytes = toUnsigned();
          unsigned long additional = detectAdditional(mainBufferPtr.front());
          if (1 + additional + bytes >= mainBufferPtr.size())
          {
            mHasCurrentNextInfo = true;
            mCurrentNextInfo.isIndefiniteLength = true;
            mCurrentNextInfo.length = bytes - mainBufferPtr.size() - (1 + additional);
            mainBufferPtr = std::string_view {};
            return false;
          }
          else
          {
            mainBufferPtr.remove_prefix(1 + additional + bytes);
          }
        }
        break;
      case Tag:
        {
          unsigned long bytes = detectAdditional(mainBufferPtr.front());
          if (1 + bytes >= mainBufferPtr.size())
            return false;
          mainBufferPtr.remove_prefix(1 + bytes);
          return next();
        }
        break;
      default:
        {
          assert(false && "not implement yet");
        }
        break;
    }
  }
  if (not mContainerStack.empty())
  {
    mContainerStack.back().cItems += 1;
  }

  return true;
}


bool CborStreamReader::isUnsigned()
{
  return type() == UnsignedInteger;
}

bool CborStreamReader::isNegative()
{
  return type() == NegativeInteger;
}

bool CborStreamReader::isByteString()
{
  return type() == ByteString;
}

bool CborStreamReader::isTextString()
{
  return type() == TextString;
}

bool CborStreamReader::isArray()
{
  return type() == Array;
}
bool CborStreamReader::isMap()
{
  return type() == Map;
}

unsigned long CborStreamReader::length()
{
  return toUnsigned();
}

unsigned long CborStreamReader::parentContainerLength()
{
  return mContainerStack.empty() ? 0 : mContainerStack.back().nItems;
}

bool CborStreamReader::isIndefiniteLength()
{
  if (!mainBufferPtr.empty())
  {
    char c = mainBufferPtr.front();

    return (c & 0b000'11111) == 0b000'11111;
  }
  return false;
}

bool CborStreamReader::isParentContainerIndefiniteLength()
{
  return mContainerStack.empty() ? false : mContainerStack.back().isIndefiniteLength;
}

CborStreamReaderError CborStreamReader::lastError()
{
  return mLastError;
}
