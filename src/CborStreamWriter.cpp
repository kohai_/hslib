#include "CborStreamWriter.hpp"
#include <cassert>
#include "HS_bit.hpp"
#include <iostream>

namespace {

  unsigned char detectAdditional(unsigned long size)
  {
    if (size < 24ul)
      return 0;
    else if ((size >> 8) == 0)
      return 1;
    else if ((size >> 16) == 0)
      return 2;
    else if ((size >> 32) == 0)
      return 4;
    else 
      return 8;
  }

  void updateCurrentContainerStack(CborStreamWriter::ContainerStack& cont)
  {
    if (cont.tags.size() > 0)
    {
      auto& top = cont.tags.top();
      if (not top.isIndefiniteLength())
      {
        if (cont.lengths.back() == 0)
        {
          top.setExceedCount();
        }
        else
        {
          cont.lengths.back() -= 1;
        }
      }
    }
  }

}


CborStreamWriter::CborStreamWriter(char* data, unsigned long size)
  : mData {data},
    mLen {size},
    mIndex {0}
{
}


void CborStreamWriter::append(unsigned long value)
{
  bool res = false;
  if ((value < 24ul) and (mIndex < mLen))
  {
    mData[mIndex] = 0b000'00000 bitor (unsigned char)value;
    mIndex += 1;
    res = true;
  }
  else if (((value >> 8) == 0) and (mIndex + 1 < mLen))
  {
    mData[mIndex] = 0b000'11000;
    *(unsigned char*) (mData + mIndex + 1) = (unsigned char)value;
    mIndex += 2;
    res = true;
  }
  else if (((value >> 16) == 0) and (mIndex + 2 < mLen))
  {
    mData[mIndex] = 0b000'11001;
    if constexpr (HS::Endian::native == HS::Endian::big)
    {
      *(unsigned short*)(mData + mIndex + 1) = (unsigned short)value;
    }
    else
    {
      *(unsigned short*)(mData + mIndex + 1) = HS::swapByteOrder((unsigned short)value);
    }
    mIndex += 3;
    res = true;
  }
  else if (((value >> 32) == 0) and (mIndex + 4 < mLen))
  {
    mData[mIndex] = 0b000'11010;
    if constexpr (HS::Endian::native == HS::Endian::big)
    {
      *(unsigned int*)(mData + mIndex + 1) = (unsigned int) value;
    }
    else
    {
      *(unsigned int*)(mData + mIndex + 1) = HS::swapByteOrder((unsigned int) value);
    }

    mIndex += 5;
    res = true;
  }
  else if (mIndex + 8 < mLen)
  {
    mData[mIndex] = 0b000'11011;
    if constexpr (HS::Endian::native == HS::Endian::big)
    {
      *(unsigned long*)(mData + mIndex + 1) = (unsigned long) value;
    }
    else
    {
      *(unsigned long*)(mData + mIndex + 1) = HS::swapByteOrder((unsigned long) value);
    }
    mIndex += 9;
    res = true;
  }

  if (res)
  {
    updateCurrentContainerStack(mContainerStack);
  }
  else
  {
    mLastError = CborStreamWriterError::OutOfMemory;
  }

}

void CborStreamWriter::append(long value)
{
  if (value >= 0)
  {
    append((unsigned long) value);
  }
  else
  {
    bool res = false;
    unsigned long uvalue = (unsigned long)(-value) - 1;
    if ((uvalue < 24ul) and (mIndex < mLen))
    {
      mData[mIndex] = 0b001'00000 bitor (unsigned char)uvalue;
      mIndex += 1;
      res = true;
    }
    else if (((uvalue >> 8) == 0) and (mIndex + 1 < mLen))
    {
      mData[mIndex] = 0b001'11000;
      *(unsigned char*) (mData + mIndex + 1) = (unsigned char)uvalue;
      mIndex += 2;
      res = true;
    }
    else if (((uvalue >> 16) == 0) and (mIndex + 2 < mLen))
    {
      mData[mIndex] = 0b001'11001;
      if constexpr (HS::Endian::native == HS::Endian::big)
      {
        *(unsigned short*)(mData + mIndex + 1) = (unsigned short)uvalue;
      }
      else
      {
        *(unsigned short*)(mData + mIndex + 1) = HS::swapByteOrder((unsigned short)uvalue);
      }
      mIndex += 3;
      res = true;
    }
    else if (((uvalue >> 32) == 0) and (mIndex + 4 < mLen))
    {
      mData[mIndex] = 0b001'11010;
      if constexpr (HS::Endian::native == HS::Endian::big)
      {
        *(unsigned int*)(mData + mIndex + 1) = (unsigned int)uvalue;
      }
      else
      {
        *(unsigned int*)(mData + mIndex + 1) = HS::swapByteOrder((unsigned int)uvalue);
      }
      mIndex += 5;
      res = true;
    }
    else if (mIndex + 9 < mLen)
    {
      mData[mIndex] = 0b001'11011;
      if constexpr (HS::Endian::native == HS::Endian::big)
      {
        *(unsigned long*)(mData + mIndex + 1) = (unsigned long)uvalue;
      }
      else
      {
        *(unsigned long*)(mData + mIndex + 1) = HS::swapByteOrder((unsigned long)uvalue);
      }
      mIndex += 9;
      res = true;
    }

    if (res)
    {
      updateCurrentContainerStack(mContainerStack);
    }
    else
    {
      mLastError = CborStreamWriterError::OutOfMemory;
    }
  }
}

void CborStreamWriter::append(CborNegativeInteger i)
{
  unsigned long value = i.value - 1;
  bool res = false;

  if ((value < 24ul) and (mIndex < mLen))
  {
    mData[mIndex] = 0b001'00000 bitor (unsigned char)value;
    mIndex += 1;
    res = true;
  }
  else if (((value >> 8) == 0) and (mIndex + 1 < mLen))
  {
    mData[mIndex] = 0b001'11000;
    *(unsigned char*) (mData + mIndex + 1) = (unsigned char)value;
    mIndex += 2;
    res = true;
  }
  else if (((value >> 16) == 0) and (mIndex + 2 < mLen))
  {
    mData[mIndex] = 0b001'11001;
    if constexpr (HS::Endian::native == HS::Endian::big)
    {
      *(unsigned short*)(mData + mIndex + 1) = (unsigned short)value;
    }
    else
    {
      *(unsigned short*)(mData + mIndex + 1) = HS::swapByteOrder((unsigned short)value);
    }
    mIndex += 3;
    res = true;
  }
  else if (((value >> 32) == 0) and (mIndex + 4 < mLen))
  {
    mData[mIndex] = 0b001'11010;
    if constexpr (HS::Endian::native == HS::Endian::big)
    {
      *(unsigned int*)(mData + mIndex + 1) = (unsigned int) value;
    }
    else
    {
      *(unsigned int*)(mData + mIndex + 1) = HS::swapByteOrder((unsigned int) value);
    }

    mIndex += 5;
    res = true;
  }
  else if (mIndex + 8 < mLen)
  {
    mData[mIndex] = 0b001'11011;
    if constexpr (HS::Endian::native == HS::Endian::big)
    {
      *(unsigned long*)(mData + mIndex + 1) = (unsigned long) value;
    }
    else
    {
      *(unsigned long*)(mData + mIndex + 1) = HS::swapByteOrder((unsigned long) value);
    }
    mIndex += 9;
    res = true;
  }

  if (res)
  {
    updateCurrentContainerStack(mContainerStack);
  }
  else
  {
    mLastError = CborStreamWriterError::OutOfMemory;
  }
}

void CborStreamWriter::append(char const* value, unsigned long len)
{
  unsigned int additionalBytes = 0;
  unsigned char additionalInfo[8] = {};

  if (len < 24) {
    additionalBytes = 0;
  }
  else if ((len >> 8) == 0) {
    additionalBytes = 1;
    additionalInfo[0] = (unsigned char) len;
  }
  else if ((len >> 16) == 0) {
    additionalBytes = 2;
    *(unsigned short*)(additionalInfo) = (unsigned short)len; }
  else if ((len >> 32) == 0) {
    additionalBytes = 4;
    *(unsigned int*)(additionalInfo) = (unsigned int)len;
  }
  else {
    additionalBytes = 8;
    *(unsigned long*)(additionalInfo) = (unsigned long)len;
  }

  unsigned long totalLength = 1 + additionalBytes + len;

  if (totalLength <= (mLen - mIndex))
  {
    switch (additionalBytes)
    {
      case 0:
        mData[mIndex] = 0b010'00000 bitor len;
        break;
      case 1:
        mData[mIndex] = 0b010'11000;
        break;
      case 2:
        mData[mIndex] = 0b010'11001;
        break;
      case 4:
        mData[mIndex] = 0b010'11010;
        break;
      case 8:
        mData[mIndex] = 0b010'11011;
        break;
      default:
        assert(0);
    }
    char* writePtr = mData + mIndex;

    mIndex += 1;
    writePtr += 1;

    for (unsigned int i = 0; i < additionalBytes; ++i)
    {
      writePtr[i] = additionalInfo[i];
    }

    mIndex += additionalBytes;
    writePtr += additionalBytes;

    for (unsigned long i = 0; i < len; ++i)
    {
      writePtr[i] = value[i];
    }

    mIndex += len;
    updateCurrentContainerStack(mContainerStack);
  }
  else
  {
    mLastError = CborStreamWriterError::OutOfMemory;
  }
}

void CborStreamWriter::append(CborTag tag)
{
  int tagValue = tag;
  unsigned char additional = detectAdditional(tagValue);

  char* mainPtr = mData + mIndex;
  bool res = false;
  if (additional == 0 and mIndex < mLen)
  {
    *mainPtr = 0b110'00000 bitor (unsigned char) tagValue;
    res = true;
  }
  else if (additional <= mLen - mIndex)
  {
    char* additionPtr = mainPtr + 1;
    switch (additional)
    {
      case 1:
        *mainPtr = 0b110'11000;
        if constexpr (not HS::isNetworkByteOrder())
        {
          *(unsigned char*) additionPtr = (unsigned char) tagValue;
        }
        else
        {
          *(unsigned char*) additionPtr = HS::swapByteOrder((unsigned char)tagValue);
        }
        break;
      case 2:
        *mainPtr = 0b110'11001;
        if constexpr (not HS::isNetworkByteOrder())
        {
          *(unsigned short*) additionPtr = (unsigned short) tagValue;
        }
        else
        {
          *(unsigned short*) additionPtr = HS::swapByteOrder((unsigned short)tagValue);
        }
        break;
      case 4:
        *mainPtr = 0b110'11010;
        if constexpr (not HS::isNetworkByteOrder())
        {
          *(unsigned int*) additionPtr = (unsigned int) tagValue;
        }
        else
        {
          *(unsigned int*) additionPtr = HS::swapByteOrder((unsigned int)tagValue);
        }
        break;
      case 8:
        *mainPtr = 0b110'11011;
        if constexpr (not HS::isNetworkByteOrder())
        {
          *(unsigned long*) additionPtr = (unsigned long) tagValue;
        }
        else
        {
          *(unsigned long*) additionPtr = HS::swapByteOrder((unsigned long)tagValue);
        }
        break;
      default:
        assert(0);
    }
    mIndex += 1 + additional;
    res = true;
  }

  if (res)
  {
    updateCurrentContainerStack(mContainerStack);
  }
  else
  {
    mLastError = CborStreamWriterError::OutOfMemory;
  }
}


void CborStreamWriter::startArray()
{
  if (mIndex < mLen)
  {
    mContainerStack.tags.push(ContainerStackItemInfo (false, true));
    char* writePtr = mData + mIndex;
    *writePtr = 0b0100'11111;
    mIndex += 1;
  }
  else
  {
    mLastError = CborStreamWriterError::OutOfMemory;
  }
}

void CborStreamWriter::startArray(unsigned long len)
{
  unsigned char additional = detectAdditional(len);
  bool enterContainer = false;
  if (additional == 0 and mIndex < mLen)
  {
    char* writePtr = mData + mIndex;
    *writePtr = 0b0100'00000 bitor (unsigned char) len;
    mIndex += 1;
    enterContainer = true;
  }

  else if (additional > 0 and additional <= mLen - mIndex)
  {
    char* mainPtr = mData + mIndex;
    char* additionPtr = mainPtr + 1;
    switch (additional)
    {
      case 1:
        *mainPtr = 0b100'11000;
        if constexpr (not HS::isNetworkByteOrder())
        {
          *(unsigned char*) additionPtr = (unsigned char) len;
        }
        else
        {
          *(unsigned char*) additionPtr = HS::swapByteOrder((unsigned char)len);
        }
        break;
      case 2:
        *mainPtr = 0b100'11001;
        if constexpr (not HS::isNetworkByteOrder())
        {
          *(unsigned short*) additionPtr = (unsigned short) len;
        }
        else
        {
          *(unsigned short*) additionPtr = HS::swapByteOrder((unsigned short)len);
        }
        break;
      case 4:
        *mainPtr = 0b100'11010;
        if constexpr (not HS::isNetworkByteOrder())
        {
          *(unsigned int*) additionPtr = (unsigned int) len;
        }
        else
        {
          *(unsigned int*) additionPtr = HS::swapByteOrder((unsigned int)len);
        }
        break;
      case 8:
        *mainPtr = 0b100'11011;
        if constexpr (not HS::isNetworkByteOrder())
        {
          *(unsigned long*) additionPtr = (unsigned long) len;
        }
        else
        {
          *(unsigned long*) additionPtr = HS::swapByteOrder((unsigned long)len);
        }
        break;
      default:
        assert(0);
    }
    mIndex += 1 + additional;
    enterContainer = true;
  }
  
  if (enterContainer)
  {
    mContainerStack.tags.push(ContainerStackItemInfo(false, false));
    mContainerStack.lengths.push_back(len);
  }
  else
  {
    mLastError = CborStreamWriterError::OutOfMemory;
  }
}

void CborStreamWriter::endArray()
{
  auto& top = mContainerStack.tags.top();
  auto& topLength = mContainerStack.lengths.back();
  if (top.isArray())
  {
    if (top.isIndefiniteLength())
    {
      if (mIndex < mLen)
      {
        char* writePtr= mData + mIndex;
        *writePtr = 0b111'11111;
        mIndex += 1;
      }
      else
      {
        mLastError = CborStreamWriterError::OutOfMemory;
      }
    }
    else if (topLength != 0 or top.isExceedCount())
    {
      mLastError = CborStreamWriterError::WrongNumberOfArrayItem;
    }
    else
    {
      mContainerStack.lengths.pop_back();
    }
    mContainerStack.tags.pop();
    updateCurrentContainerStack(mContainerStack);
  }
  else
  {
    mLastError = CborStreamWriterError::WrongNumberOfArrayItem;
  }
}

void CborStreamWriter::startMap()
{
  if (mIndex < mLen)
  {
    char* writePtr = mData + mIndex;
    *writePtr = 0b101'11111;
    mIndex += 1;
    mContainerStack.tags.push(ContainerStackItemInfo(true, true));
  }
  else
  {
    mLastError = CborStreamWriterError::OutOfMemory;
  }
}

void CborStreamWriter::startMap(unsigned long len)
{
  unsigned char additional = detectAdditional(len);
  bool enterContainer = false;
  if (additional == 0 and mIndex < mLen)
  {
    char* writePtr = mData + mIndex;
    *writePtr = 0b101'00000 bitor (unsigned char) len;
    mIndex += 1;
    enterContainer = true;
  }

  else if (additional != 0 and additional <= mLen - mIndex)
  {
    char* mainPtr = mData + mIndex;
    char* additionPtr = mainPtr + 1;
    switch (additional)
    {
      case 1:
        *mainPtr = 0b101'11000;
        if constexpr (not HS::isNetworkByteOrder())
        {
          *(unsigned char*) additionPtr = (unsigned char) len;
        }
        else
        {
          *(unsigned char*) additionPtr = HS::swapByteOrder((unsigned char)len);
        }
        break;
      case 2:
        *mainPtr = 0b101'11001;
        if constexpr (not HS::isNetworkByteOrder())
        {
          *(unsigned short*) additionPtr = (unsigned short) len;
        }
        else
        {
          *(unsigned short*) additionPtr = HS::swapByteOrder((unsigned short)len);
        }
        break;
      case 4:
        *mainPtr = 0b101'11010;
        if constexpr (not HS::isNetworkByteOrder())
        {
          *(unsigned int*) additionPtr = (unsigned int) len;
        }
        else
        {
          *(unsigned int*) additionPtr = HS::swapByteOrder((unsigned int)len);
        }
        break;
      case 8:
        *mainPtr = 0b101'11011;
        if constexpr (not HS::isNetworkByteOrder())
        {
          *(unsigned long*) additionPtr = (unsigned long) len;
        }
        else
        {
          *(unsigned long*) additionPtr = HS::swapByteOrder((unsigned long)len);
        }
        break;
      default:
        assert(0);
    }
    mIndex += 1 + additional;
    enterContainer = true;
  }

  if (enterContainer)
  {
    mContainerStack.tags.push(ContainerStackItemInfo(true, false));
    mContainerStack.lengths.push_back(len * 2);
  }

  else
  {
    mLastError = CborStreamWriterError::OutOfMemory;
  }
}

void CborStreamWriter::endMap()
{
  auto& top = mContainerStack.tags.top();
  auto& topLength = mContainerStack.lengths.back();
  if (top.isMap())
  {
    if (top.isIndefiniteLength())
    {
      if (mIndex < mLen)
      {
        char* writePtr= mData + mIndex;
        *writePtr = 0b111'11111;
        mIndex += 1;
      }
      else
      {
        mLastError = CborStreamWriterError::OutOfMemory;
      }
    }
    else if (topLength != 0 or top.isExceedCount())
    {
      mLastError = CborStreamWriterError::WrongNumberOfArrayItem;
    }
    else
    {
      mContainerStack.lengths.pop_back();
    }
    mContainerStack.tags.pop();
    updateCurrentContainerStack(mContainerStack);
  }
  else
  {
    mLastError = CborStreamWriterError::WrongNumberOfArrayItem;
  }
}

void CborStreamWriter::append(float value)
{
  bool res = false;
  if (mIndex + 5 < mLen)
  {
    char* majorPtr = mData + mIndex;
    char* additionalPtr = majorPtr + 1;

    *majorPtr = 0b111'11010;
    if constexpr (HS::Endian::native == HS::Endian::big)
    {
      *(float*)additionalPtr = value;
    }
    else
    {
      *(float*)additionalPtr = HS::swapByteOrder(value);
    }
    mIndex += 5;
    res = true;
  }

  if (res)
  {
    updateCurrentContainerStack(mContainerStack);
  }
  else
  {
    mLastError = CborStreamWriterError::OutOfMemory;
  }
}

void CborStreamWriter::append(double value)
{
  bool res = false;
  if (mIndex + 9 < mLen)
  {
    char* majorPtr = mData + mIndex;
    char* additionalPtr = majorPtr + 1;

    *majorPtr = 0b111'11011;
    if constexpr (HS::Endian::native == HS::Endian::big)
    {
      *(double*)additionalPtr = value;
    }
    else
    {
      *(double*)additionalPtr = HS::swapByteOrder(value);
    }
    mIndex += 9;
    res = true;
  }

  if (res)
  {
    updateCurrentContainerStack(mContainerStack);
  }
  else
  {
    mLastError = CborStreamWriterError::OutOfMemory;
  }
}

CborStreamWriterError CborStreamWriter::lastError() const
{
  return mLastError;
}
void CborStreamWriter::clearError()
{
  mLastError = CborStreamWriterError::Ok;
}
