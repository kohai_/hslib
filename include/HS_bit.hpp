#pragma once
namespace HS
{

  enum class Endian 
  {
#ifdef _WIN32
    little = 0,
    big = 1,
    native = little
#else
    little = __ORDER_LITTLE_ENDIAN__,
    big = __ORDER_BIG_ENDIAN__,
    native = __BYTE_ORDER__,
#endif
  };

  constexpr bool isNetworkByteOrder() noexcept
  {
    return Endian::native == Endian::big;
  }

  template<typename T>
  constexpr T swapByteOrder(T t) noexcept
  {
    T ret;
    char* src = (char*) &t;
    char* dst = (char*) &ret;

    for (int i = 0, j = sizeof(T) - 1; i < sizeof(T); ++i, --j)
    {
      dst[j] = src[i];
    }

    return ret;
  }
}
