#pragma once
#include "HS.hpp"

#include <stack>
#include <vector>

struct CborNegativeInteger
{
  explicit CborNegativeInteger(unsigned long val)
    : value {val}
  {
  }
  unsigned long value;
};

enum CborStreamWriterError
{
  Ok,
  OutOfMemory,
  WrongNumberOfArrayItem,
  MaxContainerDepth
};

enum CborTag
{
    DataTimeString,
    UnixTime,
    PositiveBignum,
    NegativeBignum,
    Decimal,
    Bigfloat,
};

class CborStreamWriter {

public:

  CborStreamWriter(char* data, unsigned long size);

   
  void append(unsigned long value);
  void append(long value);
  void append(CborNegativeInteger value);
  void append(float value);
  void append(double value);
  void append(char const* value, unsigned long len);
  void append(CborTag tag);

  void startArray();
  void startArray(unsigned long len);
  void endArray();
  void startMap();
  void startMap(unsigned long len);
  void endMap();


  void clearError();
  CborStreamWriterError lastError() const;

public:

  struct ContainerStackItemInfo
  {
    enum ContainerTag : char
    {
      FixArray,
      FixMap,
      IndefiniteArray,
      IndefiniteMap,
      FixArrayWithExceedLength,
      FixMapWithExceedLength
    };
    ContainerTag tag;

    ContainerStackItemInfo(bool isMap, bool isIndefiniteLength)
    {
      char map = (char) isMap;
      char length = (char)isIndefiniteLength * 2;

      tag = (ContainerTag) (map + length);
      
    }
    bool isArray() { return tag % 2 == 0; }
    bool isMap() { return tag % 2 == 1; }
    bool isFixLength() { return tag < 2; }
    bool isIndefiniteLength() { return tag >= 2 and tag < 4; }
    bool isExceedCount() { return tag >= 4 and tag < 6; }
    void setExceedCount() { tag = ContainerTag ((tag % 2) + 4); }
  };

  char* mData                       = nullptr;
  unsigned long mIndex              = 0ul;
  unsigned long mLen                = 0ul;
  CborStreamWriterError mLastError  = CborStreamWriterError::Ok;

  struct ContainerStack {
    std::stack<ContainerStackItemInfo> tags;
    std::vector<unsigned long> lengths;
  };

  ContainerStack mContainerStack;

};
