#pragma once

static_assert(sizeof(char) == 1);
static_assert(sizeof(unsigned char) == 1);

static_assert(sizeof(short) == 2);
static_assert(sizeof(unsigned short) == 2);

static_assert(sizeof(int) == 4);
static_assert(sizeof(unsigned int) == 4);

static_assert(sizeof(long) == 8);
static_assert(sizeof(unsigned long) == 8);

static_assert(sizeof(float) == 4);
static_assert(sizeof(double) == 8);
