#pragma once

#include <string>
#include <string_view>
#include <vector>
#include <optional>
#include <stack>


enum CborStreamReaderError
{
  NoError,
  EndOfFile,
  IndexOutOfRange,
  InvalidOperation
};

class CborStreamReader
{
public:

  enum Type {
    UnsignedInteger,
    NegativeInteger,
    ByteString,
    TextString,
    Array,
    Map,
    Tag,
    Float,
    Invalid,
  };

  struct StringResult {
    enum Status {
      EndOfString,
      Ok,
      Error
    };
    int err;
    int type;
    std::string data;
    unsigned long len;
  };


  struct TypeInfo
  {
    Type type;
    unsigned long remain;
    unsigned long remainEl;
  };
  CborStreamReader(std::string const& data);
  ~CborStreamReader();


  void addData(std::string_view data);
  void addData(std::string const& newData);
  Type type();


  bool isUnsigned();
  bool isNegative();
  bool isByteString();
  bool isTextString();
  bool isArray();
  bool isMap();
  bool isSimple();
  bool isFloat();

  unsigned long toUnsigned();
  long toNegative();

  StringResult readByteString();
  StringResult readByteString(StringResult& r);
  StringResult readTextString();
  StringResult readTextString(StringResult& r);

  bool isIndefiniteLength();
  bool isParentContainerIndefiniteLength();
  unsigned long length();
  unsigned long parentContainerLength();

  unsigned int depth();
  bool enterContainer();
  bool leaveContainer();

  bool hasNext();
  bool next(std::optional<TypeInfo> info);
  bool next();
  TypeInfo buildTypeInfo();


  CborStreamReaderError lastError();

public:
  struct ContainerStackItem {
    Type type;
    bool isIndefiniteLength;
    unsigned long nItems;
    unsigned long cItems;
  };

  struct CurrentStringNextInfo
  {
    bool isIndefiniteLength;
    unsigned long length;
    unsigned long maxLength;
  };


  struct State
  {
    Type type;
    unsigned char argumentWidth = 0u;
    unsigned long argumentValue = 0u;
    unsigned long length = 0u;
    unsigned long additional = 0u;
  };


  std::vector<ContainerStackItem> mContainerStack = {};
  CborStreamReader* tmpReader = nullptr;

  CurrentStringNextInfo mCurrentNextInfo = {};
  bool mHasCurrentNextInfo = false;

  std::string tmpBuffer;
  std::string buffer[2] = {};

  std::string_view mainBufferPtr = {};
  bool mainBuffer = false;

  CborStreamReaderError mLastError = CborStreamReaderError::NoError;

  bool  mHasStoredState = false;
  State mStoredState = {};
};
