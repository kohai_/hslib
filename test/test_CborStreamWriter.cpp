#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "CborStreamWriter.hpp"

namespace
{
  void clear(char* buffer, unsigned long len)
  {
    for (unsigned long i = 0; i < len; ++i)
    {
      buffer[i] = 0;
    }
  }
}

TEST_CASE("cbor stream writer write fix value correctly")
{
  char buffer[1024] = {};
  unsigned long len = 1024ull;
  auto writer = CborStreamWriter(buffer, len);
  
  SECTION("Write unsigned value 0")
  {
    writer.append(0ul);
    CHECK(buffer[0] == 0x00);
  }

  SECTION("Write unsigned value 5")
  {
    
    writer.append(5ul);
    CHECK(buffer[0] == 0x05);
  }

  SECTION("Write unsigned value 23 (fist byte boundary)")
  {
    writer.append(23ul);
    CHECK(buffer[0] == 0x17);
  }

  SECTION("Write unsigned value 24 (first byte boundary)")
  {
    writer.append(24ul);
    CHECK(buffer[0] == 0x18);
    CHECK(buffer[1] == 0x18);
  }

  SECTION("Write unsigned value 255 (unsigned char boundary)")
  {
    writer.append(255ul);
    CHECK(buffer[0] == 0x18);
    CHECK(buffer[1] == (char)0xff);
  }

  SECTION("Write unsigned value 256 (unsigned char boundary)")
  {
    writer.append(256ul);
    CHECK(buffer[0] == 0x19);
    CHECK(buffer[1] == (char)0x01);
    CHECK(buffer[2] == (char)0x00);
  }

  SECTION("Write unsigned value 500")
  {
    writer.append(500ul);
    CHECK(buffer[0] == 0x19);
    CHECK(buffer[1] == (char)0x01);
    CHECK(buffer[2] == (char)0xf4);
  }

  SECTION("Write unsigned value 65535 (unsigned short boundary)")
  {
    writer.append(65535ul);
    CHECK(buffer[0] == 0x19);
    CHECK(buffer[1] == (char)0xff);
    CHECK(buffer[2] == (char)0xff);
  }

  SECTION("Write unsigned value 65536 (unsigned short boundary)")
  {
    writer.append(65536ul);
    CHECK(buffer[0] == 0x1a);
    CHECK(buffer[1] == (char)0x00);
    CHECK(buffer[2] == (char)0x01);
    CHECK(buffer[3] == (char)0x00);
    CHECK(buffer[4] == (char)0x00);
  }

  SECTION("Write unsigned value 343256")
  {
    writer.append(343256ul);
    CHECK(buffer[0] == 0x1a);
    CHECK(buffer[1] == (char)0x00);
    CHECK(buffer[2] == (char)0x05);
    CHECK(buffer[3] == (char)0x3c);
    CHECK(buffer[4] == (char)0xd8);
  }

  SECTION("Write unsigned value 4294967295 (unsigned int boundary)")
  {
    writer.append(4294967295ul);
    CHECK(buffer[0] == 0x1a);
    CHECK(buffer[1] == (char)0xff);
    CHECK(buffer[2] == (char)0xff);
    CHECK(buffer[3] == (char)0xff);
    CHECK(buffer[4] == (char)0xff);
  }

  SECTION("Write unsigned value 4294967296 (unsigned int boundary)")
  {
    writer.append(4294967296ul);
    CHECK(buffer[0] == 0x1b);
    CHECK(buffer[1] == (char)0x00);
    CHECK(buffer[2] == (char)0x00);
    CHECK(buffer[3] == (char)0x00);
    CHECK(buffer[4] == (char)0x01);
    CHECK(buffer[5] == (char)0x00);
    CHECK(buffer[6] == (char)0x00);
    CHECK(buffer[7] == (char)0x00);
    CHECK(buffer[8] == (char)0x00);
  }

  SECTION("Write unsigned value 4294967291236")
  {
    writer.append(4294967291236ul);
    CHECK(buffer[0] == 0x1b);
    CHECK(buffer[1] == (char)0x00);
    CHECK(buffer[2] == (char)0x00);
    CHECK(buffer[3] == (char)0x03);
    CHECK(buffer[4] == (char)0xe7);
    CHECK(buffer[5] == (char)0xff);
    CHECK(buffer[6] == (char)0xff);
    CHECK(buffer[7] == (char)0xed);
    CHECK(buffer[8] == (char)0x64);
  }

  SECTION("Write unsigned value 18446744073709551615 (unsigned long boundary)")
  {
    writer.append(18446744073709551615ul);
    CHECK(buffer[0] == 0x1b);
    CHECK(buffer[1] == (char)0xff);
    CHECK(buffer[2] == (char)0xff);
    CHECK(buffer[3] == (char)0xff);
    CHECK(buffer[4] == (char)0xff);
    CHECK(buffer[5] == (char)0xff);
    CHECK(buffer[6] == (char)0xff);
    CHECK(buffer[7] == (char)0xff);
    CHECK(buffer[8] == (char)0xff);
  }
  
  /* Test for signed value */

  SECTION("Write signed value switch to unsigned if argument is >0")
  {
    writer.append(5ul);
    CHECK(buffer[0] == 0x05);
  }

  SECTION("Write negative value -1")
  {
    writer.append(-1l);
    CHECK(buffer[0] == 0x20);
  }

  SECTION("Write negative value -5")
  {
    writer.append(-5l);
    CHECK(buffer[0] == 0x24);
  }

  SECTION("Write negative value -23")
  {
    writer.append(-23l);
    CHECK(buffer[0] == 0x36);
  }

  SECTION("Write negative value -24 (first byte boundary)")
  {
    writer.append(-24l);
    CHECK(buffer[0] == (char) 0x37);
  }

  SECTION("Write negative value -25 (first byte boundary)")
  {
    writer.append(-25l);
    CHECK(buffer[0] == (char) 0x38);
    CHECK(buffer[1] == (char) 0x18);
  }

  SECTION("Write negative value -255")
  {
    writer.append(-255l);
    CHECK(buffer[0] == (char) 0x38);
    CHECK(buffer[1] == (char) 0xfe);
  }

  SECTION("Write negative value -256 (char boundary)")
  {
    writer.append(-256l);
    CHECK(buffer[0] == (char) 0x38);
    CHECK(buffer[1] == (char) 0xff);
  }

  SECTION("Write negative value -257 (char boundary)")
  {
    writer.append(-257l);
    CHECK(buffer[0] == (char) 0x39);
    CHECK(buffer[1] == (char) 0x01);
    CHECK(buffer[2] == (char) 0x00);
  }

  SECTION("Write negative value -3435")
  {
    writer.append(-3435l);
    CHECK(buffer[0] == (char) 0x39);
    CHECK(buffer[1] == (char) 0x0d);
    CHECK(buffer[2] == (char) 0x6a);
  }

  SECTION("Write negative value -65535 (short boundary)")
  {
    writer.append(-65535l);
    CHECK(buffer[0] == (char) 0x39);
    CHECK(buffer[1] == (char) 0xff);
    CHECK(buffer[2] == (char) 0xfe);
  }

  SECTION("Write negative value -65536 (short boundary)")
  {
    writer.append(-65536l);
    CHECK(buffer[0] == (char) 0x39);
    CHECK(buffer[1] == (char) 0xff);
    CHECK(buffer[2] == (char) 0xff);
  }

  SECTION("Write negative value -854245")
  {
    writer.append(-854245l);
    CHECK(buffer[0] == (char) 0x3a);
    CHECK(buffer[1] == (char) 0x00);
    CHECK(buffer[2] == (char) 0x0d);
    CHECK(buffer[3] == (char) 0x08);
    CHECK(buffer[4] == (char) 0xe4);
  }

  SECTION("Write negative value -4294967295 (int boundary)")
  {
    writer.append(-4294967295l);
    CHECK(buffer[0] == (char) 0x3a);
    CHECK(buffer[1] == (char) 0xff);
    CHECK(buffer[2] == (char) 0xff);
    CHECK(buffer[3] == (char) 0xff);
    CHECK(buffer[4] == (char) 0xfe);
  }

  SECTION("Write negative value -4294967296l (int boundary)")
  {
    writer.append(-4294967296l);
    CHECK(buffer[0] == (char) 0x3a);
    CHECK(buffer[1] == (char) 0xff);
    CHECK(buffer[2] == (char) 0xff);
    CHECK(buffer[3] == (char) 0xff);
    CHECK(buffer[4] == (char) 0xff);
  }

  SECTION("Write negative value -4294967297l (int boundary)")
  {
    writer.append(-4294967297l);
    CHECK(buffer[0] == (char) 0x3b);
    CHECK(buffer[1] == (char) 0x00);
    CHECK(buffer[2] == (char) 0x00);
    CHECK(buffer[3] == (char) 0x00);
    CHECK(buffer[4] == (char) 0x01);
    CHECK(buffer[5] == (char) 0x00);
    CHECK(buffer[6] == (char) 0x00);
    CHECK(buffer[7] == (char) 0x00);
    CHECK(buffer[8] == (char) 0x00);
  }

  SECTION("Write negative value -429344967297l")
  {
    writer.append(-429344967297l);
    CHECK(buffer[0] == (char) 0x3b);
    CHECK(buffer[1] == (char) 0x00);
    CHECK(buffer[2] == (char) 0x00);
    CHECK(buffer[3] == (char) 0x00);
    CHECK(buffer[4] == (char) 0x63);
    CHECK(buffer[5] == (char) 0xf6);
    CHECK(buffer[6] == (char) 0xf4);
    CHECK(buffer[7] == (char) 0x4a);
    CHECK(buffer[8] == (char) 0x80);
  }

  // CborStreamWriter::append(long) can only handle to
  // MIN(long) value, further negative value should use
  // CborStreamWriter::append(QborNegativeInteger)
  SECTION("Write negative value -9223372036854775807 (long boundary)")
  {
    writer.append(-9223372036854775807);
    CHECK(buffer[0] == (char) 0x3b);
    CHECK(buffer[1] == (char) 0x7f);
    CHECK(buffer[2] == (char) 0xff);
    CHECK(buffer[3] == (char) 0xff);
    CHECK(buffer[4] == (char) 0xff);
    CHECK(buffer[5] == (char) 0xff);
    CHECK(buffer[6] == (char) 0xff);
    CHECK(buffer[7] == (char) 0xff);
    CHECK(buffer[8] == (char) 0xfe);
  }


  SECTION("Write negative value using CborNegativeInteger should able to write -9223372036854775808")
  {
    writer.append(CborNegativeInteger(9223372036854775808ul));
    CHECK(buffer[0] == (char) 0x3b);
    CHECK(buffer[1] == (char) 0x7f);
    CHECK(buffer[2] == (char) 0xff);
    CHECK(buffer[3] == (char) 0xff);
    CHECK(buffer[4] == (char) 0xff);
    CHECK(buffer[5] == (char) 0xff);
    CHECK(buffer[6] == (char) 0xff);
    CHECK(buffer[7] == (char) 0xff);
    CHECK(buffer[8] == (char) 0xff);
  }

  SECTION("Write negative value using CborNegativeInteger should write more negative -18446744073709551615")
  {
    writer.append(CborNegativeInteger(18446744073709551615ul));
    CHECK(buffer[0] == (char) 0x3b);
    CHECK(buffer[1] == (char) 0xff);
    CHECK(buffer[2] == (char) 0xff);
    CHECK(buffer[3] == (char) 0xff);
    CHECK(buffer[4] == (char) 0xff);
    CHECK(buffer[5] == (char) 0xff);
    CHECK(buffer[6] == (char) 0xff);
    CHECK(buffer[7] == (char) 0xff);
    CHECK(buffer[8] == (char) 0xfe);
  }
//TODO (hanasou): firgue out how to write 18446744073709551616 using the same api

  /* Test for tag value */

  /* Test for double value */

  SECTION("Write value 1.54")
  {
    writer.append(1.54);
    CHECK(buffer[0] == (char) 0xfb);
    CHECK(buffer[1] == (char) 0x3f);
    CHECK(buffer[2] == (char) 0xf8);
    CHECK(buffer[3] == (char) 0xa3);
    CHECK(buffer[4] == (char) 0xd7);
    CHECK(buffer[5] == (char) 0x0a);
    CHECK(buffer[6] == (char) 0x3d);
    CHECK(buffer[7] == (char) 0x70);
    CHECK(buffer[8] == (char) 0xa4);
  }
}

TEST_CASE("CborStreamWriter move mIndex correctly")
{
  char buffer[1024] = {};
  unsigned long len = 1024ull;
  auto writer = CborStreamWriter(buffer, len);

  REQUIRE(writer.mIndex == 0);
  SECTION("Writer move index fix width value")
  {
    writer.append(25ul);
    CHECK(writer.mIndex == 2);
  }

  SECTION("Writer move index when start indefinite length Array")
  {
    unsigned int previous = writer.mIndex;
    writer.startArray();
    CHECK(writer.mIndex == previous + 1);
  }

  SECTION("Writer move index when start normal Array")
  {
    unsigned int previous = writer.mIndex;
    writer.startArray(500);
    CHECK(writer.mIndex == previous + 3);
  }

}

TEST_CASE("CborStreamWriter write map correctly")
{
  char buffer[1024] = {};
  unsigned long len = 1024ull;
  auto writer = CborStreamWriter(buffer, len);

  SECTION("Check number of element in map")
  {
    writer.startMap(3);
    CHECK(writer.mContainerStack.lengths.back() == 6);
  }

}

TEST_CASE("CborStreamWriter handle error correctly")
{

  SECTION("Write fix data without memory")
  {
    char buffer[0] = {};
    unsigned long len = 0ul;
    auto writer = CborStreamWriter(buffer, len);

    writer.clearError();
    
    writer.append(5ul);


    CHECK(writer.lastError() != CborStreamWriterError::Ok);
  }

  SECTION("Write dynamic data without memory")
  {
    char buffer[0] = {};
    unsigned long len = 0ul;
    auto writer = CborStreamWriter(buffer, len);

    writer.clearError();
    
    writer.startArray(); CHECK(writer.lastError() != CborStreamWriterError::Ok);
  }

  SECTION("Write multiple error")
  {
    char buffer[0] = {};
    unsigned long len = 0ul;
    auto writer = CborStreamWriter(buffer, len);

    writer.clearError();
    
    writer.startArray();
    writer.startMap(6ul);
    writer.startArray(6ul);
    CHECK(writer.lastError() != CborStreamWriterError::Ok);
  }

  SECTION("Write mismatch number of element in an array")
  {
    char buffer[1024] = {};
    unsigned long len = 1024ul;
    auto writerc = CborStreamWriter(buffer, len);

    writerc.clearError();

    writerc.startArray(6ul);
    {
      writerc.append(6ul);
      writerc.append(-1l);
      writerc.append(6ul);
    }
    writerc.endArray();

    CHECK(writerc.lastError() != CborStreamWriterError::Ok);
  }

  SECTION("Write mismatch number of element in an map")
  {
    char buffer[1024] = {};
    unsigned long len = 1024ul;
    auto writerc = CborStreamWriter(buffer, len);

    writerc.clearError();

    writerc.startMap(6ul);
    {
      writerc.append(6ul);
      writerc.append(-1l);
      writerc.append(6ul);
    }
    writerc.endMap();

    CHECK(writerc.lastError() != CborStreamWriterError::Ok);
  }
}
