#include "CborStreamReader.hpp"
#include "CborStreamWriter.hpp"

#include <catch2/catch_test_macros.hpp>

#include <QCborStreamReader>
#include <QCborStreamWriter>
#include <QByteArray>
#include <QDateTime>

#include <iostream>

TEST_CASE("CborStreamReader decode fix data correctly")
{
  auto qtData = QByteArray();
  auto qtWriter = QCborStreamWriter(&qtData);

  SECTION("Read unsigned value 0")
  {
    qtWriter.append(0u);

    auto reader = CborStreamReader(qtData.toStdString());

    CHECK(reader.toUnsigned() == 0u);
  }

  SECTION("Read unsigned value 6")
  {
    qtWriter.append(6u);

    auto reader = CborStreamReader(qtData.toStdString());

    CHECK(reader.toUnsigned() == 6u);
  }

  SECTION("Read unsigned value 23 (first byte boundary)")
  {
    qtWriter.append(23u);

    auto reader = CborStreamReader(qtData.toStdString());

    CHECK(reader.toUnsigned() == 23u);
  }

  SECTION("Read unsigned value 24 (first byte boundary)")
  {
    qtWriter.append(24u);

    auto reader = CborStreamReader(qtData.toStdString());

    CHECK(reader.toUnsigned() == 24u);
  }

  SECTION("Read unsigned value 25 (first byte boundary)")
  {
    qtWriter.append(25u);

    auto reader = CborStreamReader(qtData.toStdString());

    CHECK(reader.toUnsigned() == 25u);
  }

  SECTION("Read unsigned value of 71")
  {
    qtWriter.append(71u);
    auto reader = CborStreamReader(qtData.toStdString());
    CHECK(reader.toUnsigned() == 71u);
  }
}

TEST_CASE("CborStreamReader not advance index if wanted to")
{
  auto data = QByteArray();
  auto writer = QCborStreamWriter(&data);

  writer.startArray(3);
  {
    writer.append(1u);
    writer.append(2u);
    writer.append(3u);
  }
  writer.endArray();


  auto reader = CborStreamReader(data.toStdString());

  REQUIRE(reader.length() == 3);
  auto index0 = reader.mainBufferPtr;

  CHECK(reader.length() == 3);
  auto index1 = reader.mainBufferPtr;
  CHECK(index1 == index0);
}



TEST_CASE("CborStreamReader is able to recover from error 1")
{
  auto qtData = QByteArray();
  auto qtWriterDevice = QCborStreamWriter(&qtData);

  qtWriterDevice.startArray(3);
  qtWriterDevice.append(5u);
  qtWriterDevice.append(5u);
  qtWriterDevice.startArray(2);
  qtWriterDevice.append(5u);

  REQUIRE(qtData.size() == 5);

  auto reader = CborStreamReader(qtData.toStdString());


  REQUIRE(reader.isArray());
  reader.enterContainer();

  bool breakFlag = false;
  int nIteration = 0;
  while (not breakFlag and reader.hasNext())
  {
    if (not reader.next())
    {
      breakFlag = true;
    }
    nIteration += 1;
  }

  REQUIRE(breakFlag == true);
  REQUIRE(nIteration == 3);


  auto additionalQtData = QByteArray();
  auto additionalQtWriter = QCborStreamWriter(&additionalQtData);
  additionalQtWriter.append(24u);
  additionalQtWriter.endArray();
  additionalQtWriter.endArray();
  reader.addData(additionalQtData.toStdString());

  while (reader.hasNext())
  {
    CHECK(reader.next() == true);
    nIteration += 1;
  }
  CHECK(reader.leaveContainer());
}

TEST_CASE("CborStreamReader reads payload 2")
{
  auto data = QByteArray();
  auto writer = QCborStreamWriter(&data);

  auto theLongTokenValue = std::string("r8e8wurhads,msc10ru384u32irtjwelkafjal01294810941jqewhfjasdfjasdfja1229");
  auto title = std::wstring(L"title");
  auto body = std::wstring(L"body");
  auto created = std::wstring(L"created");
  auto updated = std::wstring(L"updated");
  writer.startMap();
  {

    writer.append(QByteArray("command"));
    writer.append(5u);

    writer.append(QByteArray("token"));
    writer.append(QByteArray(theLongTokenValue.data()));

    writer.append(QByteArray("attributes"));
    writer.startMap(4);
    {
      writer.append("title");
      writer.append("JSON:API paints my bikeshed!");

      writer.append(("body"));
      writer.append(("A short body"));

      writer.append("created");
      writer.append(QCborKnownTags::DateTimeString);
      writer.append(QDateTime::currentDateTime().toString("yyyy-MM-ddThh:mm:ss.zZ"));

      writer.append("updated");
      writer.append(QCborKnownTags::DateTimeString);
      writer.append(QDateTime::currentDateTime().toString("yyyy-MM-ddThh:mm:ss.zZ"));
    }
    writer.endMap();

    writer.append("additional");
    writer.startArray();
    {
      for (int i = 0; i < 10; ++i)
      {
        writer.append(i);
      }
    }
    writer.endArray();
  }
  writer.endMap();


  auto reader = CborStreamReader(data.toStdString());

  CHECK(reader.isMap());
  CHECK(reader.isIndefiniteLength());

  CHECK(reader.enterContainer());

  int i = 0;
  while (reader.hasNext())
  {
    CHECK(reader.isByteString());
    auto key = std::string();

    auto res = reader.readByteString();
    while (res.err == CborStreamReader::StringResult::Status::Ok)
    {
      key.append(res.data);
      res = reader.readByteString(res);
    }

    CHECK(res.err == CborStreamReader::StringResult::Status::EndOfString);
    key.append(res.data);

    if (key == "command")
    {
      REQUIRE(reader.isUnsigned());

      unsigned long code = reader.toUnsigned();


      CHECK(code == 5u);
      CHECK(reader.next());

      CHECK(reader.lastError() == CborStreamReaderError::NoError);


    }
    else if (key == "token")
    {
      std::cout << "A token\n";
      REQUIRE(reader.isByteString());
      CHECK(reader.length() == theLongTokenValue.length());
      auto res = reader.readByteString();
      auto tokenValue = std::string();

      while (res.err == CborStreamReader::StringResult::Status::Ok)
      {
        tokenValue.append(res.data);
        res = reader.readByteString(res);
      }
      CHECK(res.err == CborStreamReader::StringResult::Status::EndOfString);
      tokenValue.append(res.data);

      CHECK(tokenValue == theLongTokenValue);


    }
    else if (key == "attributes")
    {
      std::cout << "A attributes\n";
      REQUIRE(reader.isMap());
      CHECK(reader.length() == 4);

      CHECK(reader.enterContainer());
      CHECK(reader.depth() == 2);

      CHECK(reader.isTextString());
      CHECK(reader.length() == title.length());

      while (reader.hasNext())
      {
        auto res = reader.readTextString();
        auto attrKey = std::string();

        while (res.err == CborStreamReader::StringResult::Status::Ok)
        {
          attrKey.append(res.data);
        }

        CHECK(res.err == CborStreamReader::StringResult::Status::EndOfString);
        attrKey.append(res.data);
        std::cout << attrKey << std::endl;


        auto attrKeyW = QString::fromStdString(attrKey);

        if (attrKeyW == QString("title"))
        {
        }
        else if (attrKeyW == QString("body"))
        {
        }
        else if (attrKeyW == QString("created"))
        {
        }
        else if (attrKeyW == QString("updated"))
        {
        }
        else
        {
          REQUIRE(false);
        }

        reader.next();



      }


    }
    else
    {
      std::cout << key << std::endl;
      CHECK(false);
    }


    i++;

    if (i == 5)
      break;

  }

}
